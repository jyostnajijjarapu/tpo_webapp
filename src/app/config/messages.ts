export const messages = {
    loadingDataMsg: 'Loading data...',
    serverErrorMsg: 'Something went wrong. Try again later',
    sessionExpMsg: 'Your session expired. Please log in again.',
    sessionExpHeading: 'Session Expiry',
    logoutMsg: 'Are you sure you want to logout?',
    logoutHeadingMsg: 'Logout',
    deactivateConfirmMsg: 'You have unsaved changes. Are you sure you want to leave?',
    confirmHeading: 'Confirm!',
    selectProperFileMsg: 'Please select CSV / JSON file',
    selectFileMsg: 'Please select file',
    noDataInFileMsg: 'File does not contain data',
    fieldRequiredMsg: 'This field is required',
};