import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {
  fileInfo = '';
  inputFilePath = '';
  constructor() { }
  // select file
  selectFile(event: any): object {

    const selectedFiles = event.target.files;
    if (selectedFiles && selectedFiles.length) {
      this.fileInfo = selectedFiles[0];
      this.inputFilePath = event.target.value;
      console.log(this.inputFilePath)
    }
    return { fileInfo: this.fileInfo, inputFilePath: this.inputFilePath };
  }
}
