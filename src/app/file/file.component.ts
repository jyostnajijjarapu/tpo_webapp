import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FileUploadService } from '../services/file-upload.service';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.scss']
})
export class FileComponent implements OnInit {
  inputFilePath = '';
  fileInfo: any;
  constructor(private router: Router, private fileuploadservice: FileUploadService) { }

  ngOnInit(): void {
  }
  // select file
  selectFile(event: any): void {
    const data: any = this.fileuploadservice.selectFile(event);
    this.fileInfo = data.fileInfo;
    this.inputFilePath = data.inputFilePath;
    console.log(this.inputFilePath)

  }



  fileSubmit() {
    this.router.navigate(['/promotion']);
  }
}
