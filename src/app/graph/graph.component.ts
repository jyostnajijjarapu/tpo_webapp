import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3';
import * as d3Shape from 'd3-shape';
import { FileUploadService } from '../services/file-upload.service';
@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss']
})
export class GraphComponent implements OnInit {
  inputFilePath = '';
  fileInfo: any;
  fetchedData: any = [];
  constructor(private fileuploadservice: FileUploadService) {
    const data: any = this.fileuploadservice.selectFile(event);
    console.log(data);
    this.fileInfo = data.fileInfo;
    this.inputFilePath = data.inputFilePath;
    console.log(this.inputFilePath);
  }


  data = [
    { weeknumber: 10, totalsales: 10000 },
    { weeknumber: 15, totalsales: 15000 },
    { weeknumber: 18, totalsales: 18000 },
    { weeknumber: 20, totalsales: 30000 },
    { weeknumber: 24, totalsales: 10000 },
    { weeknumber: 29, totalsales: 40000 },
    { weeknumber: 30, totalsales: 20000 },
    { weeknumber: 35, totalsales: 10000 },
    { weeknumber: 42, totalsales: 30000 },
    { weeknumber: 48, totalsales: 25000 },
    { weeknumber: 56, totalsales: 40000 },
    { weeknumber: 60, totalsales: 36000 },
    { weeknumber: 67, totalsales: 27000 },
    { weeknumber: 70, totalsales: 21000 },
    { weeknumber: 80, totalsales: 35000 },
    { weeknumber: 88, totalsales: 12000 },
    { weeknumber: 90, totalsales: 18000 },
    { weeknumber: 93, totalsales: 28000 },
    { weeknumber: 98, totalsales: 20000 },
    { weeknumber: 100, totalsales: 40000 },
    { weeknumber: 105, totalsales: 32000 },
    { weeknumber: 110, totalsales: 22000 },
    { weeknumber: 120, totalsales: 17000 },
    { weeknumber: 130, totalsales: 34000 },
    { weeknumber: 138, totalsales: 28000 },




  ];
  data1 = [
    { weeknumber: 140, totalsales: 24000 },
    { weeknumber: 142, totalsales: 32000 },
    { weeknumber: 150, totalsales: 16000 },
    { weeknumber: 155, totalsales: 40000 },
    { weeknumber: 160, totalsales: 28000 },


  ]

  line: any;
  x: any;
  y: any;
  svg: any;
  getdata: any = [];
  date1: any = [];
  ngOnInit() {



    // let csvToJson = require('convert-csv-to-json');

    // let fileInputName = 'final_data_1.csv';
    // let fileOutputName = 'myOutputFile.json';

    // csvToJson.generateJsonFileFromCsv(fileInputName, fileOutputName);
    // let json = csvToJson.getJsonFromCsv("myInputFile.csv");
    // for (let i = 0; i < json.length; i++) {
    //   console.log(json[i]);
    // }

    // d3.csv("/assets/final_data_1.csv").then(data1 => {
    //   this.fetchedData = data1;
    //   console.log(data1);
    //   console.log("fetch", this.fetchedData)
    //   for (var i = 0; i < data1.length; i++) {

    //     console.log(data1[i].date2);
    //     console.log(data1[i].Baseline_Volume);
    //     this.getdata.push({
    //       date: data1[i].date2,
    //       volume: data1[i].Baseline_Volume
    //     })
    //     // this.date.push(data1[i].Baseline_Volume)

    //   }
    //   console.log("date", this.getdata);
    //   console.log("volume", this.getdata[0].volume)
    //   for (var i = 0; i < this.getdata.length; i++) {
    //     this.date1.push({
    //       dates: this.getdata[i].date,
    //       volumes: this.getdata[i].volume
    //     })
    //   }
    //   console.log("vhgsdchgvh", this.date1)
    //   console.log(this.date1.volume)
    // })
    this.drawline();


  }
  //Draw a line
  private drawline() {
    var margin = { top: 50, right: 50, bottom: 50, left: 150 },
      width = 1200 - margin.left - margin.right,
      height = 500 - margin.top - margin.bottom;
    //append svg with id   
    this.svg = d3.select("#timeseries")
      .append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform",
        "translate(" + margin.left + "," + margin.top + ")");
    //x-scalelinear
    this.x = d3.scaleLinear()
      .domain([0, 160])
      .range([0, width]);
    //append the text for x-axis name
    this.svg.append("text")
      .attr("transform",
        "translate(" + (width / 2) + " ," +
        (height + margin.top) + ")")
      .style("text-anchor", "middle")
      .text("Date");

    this.svg.append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(this.x));
    // var minvalue: any = d3.min(this.getdata, function (d: any) { return +d.volume })
    // var maxvalue: any = d3.max(this.getdata, function (d: any) { return +d.volume })
    // console.log(minvalue);
    // console.log(maxvalue);
    //y-scalelinear
    this.y = d3.scaleLinear()
      .domain([8000, 40000])
      .range([height, 0]);

    //append the text for y-axis name

    this.svg.append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 60 - margin.left)
      .attr("x", 0 - (height / 2))
      .attr("dy", "1.7em")
      .style("text-anchor", "middle")
      .text("Volume");

    this.svg.append("g")
      .call(d3.axisLeft(this.y));


    this.line = d3Shape.line()
      .x((d: any) => this.x(d.weeknumber))
      .y((d: any) => this.y(d.totalsales))
      .curve(d3.curveMonotoneX);


    // // Show confidence interval
    // this.svg.append("path")
    //   .datum(this.getdata)
    //   .attr("fill", "#cce5df")
    //   .attr("stroke", "none")
    //   .attr("d", d3.area()
    //     .x((d: any) => this.x(d.date))
    //     .y0((d: any) => this.y(10000))
    //     .y1((d: any) => this.y(d.volume))
    //   )

    //append path
    this.svg.append('path')
      .datum(this.data)
      .attr('class', 'line')
      .attr('d', this.line)
      .attr("stroke", "steelblue")
      .style("stroke-width", 1)
      .style("fill", "none");

    //append path   
    this.svg.append('path')
      .datum(this.data1)
      .attr('class', 'curve')
      .attr('d', this.line)
      .attr('stroke', "purple")
      .style("stroke-width", 1)
      .style("fill", "none");
    //append circle
    this.svg
      .selectAll('circle')
      .data(this.data)
      .enter()
      .append('circle')
      .attr("cx", (d: any) => this.x(d.weeknumber))
      .attr("cy", (d: any) => this.y(d.totalsales))
      .attr("r", 2)
      .style("fill", "black")
    //append circle
    this.svg
      .selectAll('dot')
      .data(this.data1)
      .enter()
      .append('circle')
      .attr("cx", (d: any) => this.x(d.weeknumber))
      .attr("cy", (d: any) => this.y(d.totalsales))
      .attr("r", 2)
      .style("fill", "black")
  }





}
