import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { messages } from '../config/messages';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  required = messages.fieldRequiredMsg;
  loginUser: any;
  loginForm: FormGroup;
  isSubmitted = false;
  constructor(private fb: FormBuilder, private router: Router) {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  // login handler
  loginHandler() {
    if (this.loginForm.valid) {
      this.isSubmitted = true;
      console.log("login successfully");
      this.router.navigate(['/file']);
    }
    this.isSubmitted = false;
    const logindetails = this.loginForm.value;
    localStorage.setItem('currentUser', logindetails.username);
  }



}
