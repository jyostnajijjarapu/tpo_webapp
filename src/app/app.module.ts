import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FileComponent } from './file/file.component';
import { LoginComponent } from './login/login.component';
import { MaterialModule } from './material/material.module';
import { PromotionComponent } from './promotion/promotion.component';
import { GraphComponent } from './graph/graph.component';
import { TimeComponent } from './time/time.component';
@NgModule({
  declarations: [
    AppComponent,
    FileComponent,
    LoginComponent,
    PromotionComponent,
    GraphComponent,
    TimeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
